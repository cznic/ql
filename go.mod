module modernc.org/ql

go 1.21

require (
	modernc.org/b v1.1.0
	modernc.org/db v1.0.13
	modernc.org/file v1.0.9
	modernc.org/golex v1.1.0
	modernc.org/internal v1.1.1
	modernc.org/lldb v1.0.8
	modernc.org/mathutil v1.7.1
	modernc.org/strutil v1.2.1
)

require (
	github.com/edsrzf/mmap-go v1.2.0 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/sys v0.28.0 // indirect
	modernc.org/fileutil v1.3.0 // indirect
	modernc.org/sortutil v1.1.1 // indirect
	modernc.org/zappy v1.0.9 // indirect
)
